class Article < ApplicationRecord
  searchkick

  def self.sql_search(q)
    if q
      articles = Article.where('lower(text) LIKE lower(?)', "%#{q}%")
    else
      articles = []
    end
    articles
  end
end
