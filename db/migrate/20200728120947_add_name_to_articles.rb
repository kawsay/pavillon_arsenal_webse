class AddNameToArticles < ActiveRecord::Migration[6.0]
  def change
    add_column :articles, :name, :text
  end
end
