class ExtractTextFromPdfWorker
  include Sidekiq::Worker

  def perform(id, path)
    pdf = Pdf.find(id)

    if File.exists?(path) && pdf.status == 'uploaded'
      file_path = Rails.root.join("resources/txt/pdfs/#{id}")

      pdf.update!(
        status: :being_processed
      )

      begin
        Docsplit.extract_text(path, ocr: true, output: file_path)
      rescue
        pdf.update!(
          status: :error
        )
        return nil
      end

      output_file = Dir["#{file_path}/*.txt"][0]
      pdf.update!(
        text:   File.open(output_file).read,
        status: :extracted
      )
    end
  end
end

