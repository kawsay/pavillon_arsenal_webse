require 'sidekiq/web'

Rails.application.routes.draw do
  root to: 'articles#index'

  resources :pdfs
  resources :articles

  mount Sidekiq::Web => '/sidekiq'
end
