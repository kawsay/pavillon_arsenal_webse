json.extract! pdf, :id, :title, :file, :created_at, :updated_at
json.url pdf_url(pdf, format: :json)
