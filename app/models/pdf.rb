class Pdf < ApplicationRecord
  enum status: [:uploaded, :being_processed, :extracted, :error]

  has_one_attached :file

  after_create :set_status
  after_commit :extract_text_callback

  validates :file, presence: true, blob: { content_type: 'application/pdf' }
  validates :title, presence: true

  #TODO: Refactor into a dedicated search module
  def self.search(q)
    if q
      pdfs = Pdf.where('lower(text) LIKE lower(?)', "%#{q}%")
    else
      pdfs = []
    end
    pdfs
  end

  def get_text_samples(q)
    words = self.text.split(' ')

    begin
      idx = find_occurrences_indexes(q, words)

      start_idx = idx.map { |i| find_sentence_begining(words, i) }

      samples = idx.map do |i|
        start_idx.map do |s_i|
          extract_sentences(words, s_i, i)
        end
      end

    rescue
      ['Unknown error, my script sucks ¯\_(ツ)_/¯']
    end

    samples.flatten.uniq
  end

  def find_occurrences_indexes(q, words)
    words.each_index.select { |i| /#{q}/.match(words[i]) }
  end

  def find_sentence_begining(words, i)
    while true
      return i + 1  if /.*[\.\!\?]$/.match words[i]
      i -= 1
    end
  end

  def extract_sentences(words, start_idx, idx)
    words[start_idx..(idx + 100)].join(' ')
  end
  #####################
  



  def set_status
    self.update!(status: :uploaded) if self.file.present?
  end

  def extract_text_callback
    ExtractTextFromPdfWorker.perform_async(self.id, attachment_local_path)
  end

  def attachment_local_path
    ActiveStorage::Blob.service.send(:path_for, self.file.key)
  end
end
