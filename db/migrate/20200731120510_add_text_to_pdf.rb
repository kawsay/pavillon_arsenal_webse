class AddTextToPdf < ActiveRecord::Migration[6.0]
  def change
    add_column :pdfs, :text, :string
  end
end
