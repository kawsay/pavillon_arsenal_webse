class AddStatusToPdfs < ActiveRecord::Migration[6.0]
  def change
    add_column :pdfs, :status, :integer
  end
end
