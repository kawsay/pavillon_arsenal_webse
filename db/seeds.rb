Article.all.destroy_all

files = Dir[File.join(Rails.root, 'resources/txt/*')]

files.each do |file|
  lines = File.readlines(file)
  Article.create!(
    url: lines[0][0..-2],
    text: lines[1][0..-2],
    name: lines[0][0..-2].split('/').last.match(/[[^\d\W]+\-]+/)[0].gsub('-', ' ').strip.capitalize
  )
end
